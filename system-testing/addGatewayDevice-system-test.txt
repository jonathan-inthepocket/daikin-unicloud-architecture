@startuml

!includeurl https://raw.githubusercontent.com/inthepocket/plantuml-styles/master/styles.plantuml!0
skinparam maxmessagesize 250

    participant "Runscope"
    participant "Cloud Platform"

group Add Gateway Device System Test
    "Runscope" -> "Cloud Platform" : GET /gateway-devices

    "Runscope" -> "Runscope": expect gateway device and its management points
end

@enduml