@startuml

!includeurl https://raw.githubusercontent.com/inthepocket/plantuml-styles/master/styles.plantuml!0
skinparam maxmessagesize 250

actor "App"
box "Daikin Unified Cloud"
    participant "API Gateway"
	participant "User Site Service"
end box
    participant "Cognito User Service"
group Occurs once at initialisation
    "API Gateway" -> "Cognito User Service" : get JWK set

    "Cognito User Service" --> "API Gateway" : JWK set

    "API Gateway" -> "API Gateway" : store JWK set in cache
end

"App" -> "API Gateway" : /sites [jwt-token]

"API Gateway" -> "API Gateway" : extract idToken from \njwt-token in header

"API Gateway" -> "API Gateway" : verify token by JWK set

"API Gateway" -> "User Site Service" : /sites [jwt-token]

"User Site Service" -> "User Site Service" : extract userIdFromIdToken

"User Site Service" -> "User Site Service" : get persisted SiteIds linked to the user

"User Site Service" --> "App" : List of Sites

@enduml