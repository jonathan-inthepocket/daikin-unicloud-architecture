@startuml

!includeurl https://raw.githubusercontent.com/inthepocket/plantuml-styles/master/styles.plantuml!0
skinparam maxmessagesize 250

actor "App"
box "Daikin Unified Cloud"
	participant "User Site Service"
    participant "Device Service"
end box
    participant "Cognito User Service"
group Occurs once at initialisation of the User Site Service
    "User Site Service" -> "Cognito User Service" : get JWK set

    "Cognito User Service" --> "User Site Service" : JWK set

    "User Site Service" -> "User Site Service" : store JWK set in cache
end

"App" -> "User Site Service" : /gateway-devices [jwt-token]

"User Site Service" -> "User Site Service" : extract idToken from \njwt-token in header

"User Site Service" -> "User Site Service" : verify token by JWK set

"User Site Service" -> "User Site Service" : extract userIdFromIdToken

"User Site Service" -> "User Site Service" : get persisted SiteIds with gatewayDeviceIds linked to the user

"User Site Service" -> "Device Service": /gateway-devices?id={gatewayDeviceId1}&id={gatewayDeviceId2}

"Device Service" --> "User Site Service": List of GatewayDevices with managementPoints

"User Site Service" --> "App" : List of Sites with GatewayDevices and managementPoints

@enduml