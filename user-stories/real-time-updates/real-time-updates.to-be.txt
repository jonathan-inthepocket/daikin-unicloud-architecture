@startuml

!includeurl https://raw.githubusercontent.com/inthepocket/plantuml-styles/master/styles.plantuml!0
skinparam maxmessagesize 250
skinparam backgroundcolor white

    box "Model Service X"
    participant "Model X"
    participant "SNS Topic: model-state"
    end box
    box "Device Service"
    participant "Device Service"
    database "Device Collection (Mongo)"
    database "User-Site-GatewayDevice Collection (Mongo)"
    database "Connections / GatewayDevice (DynamoDB)"
    end box
    participant "Api Gateway"
    actor "App"


group App connects via Websockets
    "App" -> "Api Gateway" : connect wss://

    "Api Gateway" -> "Device Service": POST /websocket/connection (connectionId, userId)

    "Device Service" -> "User-Site-GatewayDevice Collection (Mongo)" : GET devices for userId

    "Device Service" -> "Connections / GatewayDevice (DynamoDB)" : INSERT links deviceId / connectionId
end
== ==
group Model sends an update

    "Model X" --> "SNS Topic: model-state" : Updated DeuceMessage (messageType: update)

    "Device Service" -> "SNS Topic: model-state" : Consume DeuceMessage

    "Device Service" -> "Device Collection (Mongo)" : UPDATE Device State

    "Device Service" -> "Connections / GatewayDevice (DynamoDB)": GET connectionIds for gatewayDevice

    group if has current connections

        "Device Service" -> "Device Service" : calculate diff with old state

        group for each updated characteristic
            group for each connectionId
                "Device Service" -> "Api Gateway": Post to Connection
                "Api Gateway" --> "App": publish via ws
            end
        end
    end
end

@enduml