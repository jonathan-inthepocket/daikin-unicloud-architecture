This repository is deprecated in favor of [this one](https://bitbucket.org/inthepocket/daikin-unicloud-architecture/src/master/)

# Daikin Unicloud Architecture

Here you can find the Plant UML Diagrams related to the Daikin Unicloud project

> Note that the most up-to-date schemas can always be found on Confluence. This is currently used as a backup for Confluence and to use the VSCode PlantUML plugin

## Workflow

Before editing an existing diagram, make sure to always start from the one in Confluence. When finished, copy/paste your new diagram to Confluence.

## Link to Confluence

All sequence diagrams can be found [here](https://confluence.itpservices.be/display/DAIKINCLOUD/System+Process+View)